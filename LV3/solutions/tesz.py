# -*- coding: utf-8 -*-
"""
Created on Wed Dec  1 18:08:56 2021

@author: student
"""
import pandas as pd 
mtcars = pd.read_csv('../resources/mtcars.csv') 
print(mtcars[(mtcars.am == 1) & (mtcars.hp > 120)].car) 

print(mtcars.iloc[1:3, 5:10]) 
print(mtcars.iloc[:, [0,4,7]]) 